from steem.account import Account
from steem.post import Post
from steem import Steem
from datetime import datetime
import requests

global version
version = 0.5

# Boot
def boot():
	print("######### SteemPostIta Bot #########")
	print("#    Made with love by @ilvacca    #")
	print("###############v.%s################\n"%(version))

# Log del time per i print
def time_log():
	time = datetime.now().strftime("%H:%M:%S")
	return time + " - "

# Log della data attuale
def today():
	today = datetime.now().strftime("%Y%m%d")
	return today

# Classe "master_user" ovvero l'utilizzatore del bot
class master_user:

	# Inizializzazione dell'istanza "master_user"
	def __init__(self, username):
		self.username = username
		# Lista di tutti gli account osservati dal "master_user"
		self.account_list = []
		# Lista di tutti i post votabili dal "master_user"
		self.master_votable_post_list = []
		# Bilancio degli SBD
		self.sbd_balance = Account(self.username)['sbd_balance']
		# Lista dei followers del "master_user"
		self.followers_list = []
		self.login()

	# Testa la connessione del pc pingando "google.com"
	def test_connection(self):
		if requests.get('http://www.google.com').status_code == 200:
			return True
		else:
			print("Problem with internet connection. Retrying...")
			self.test_connection()

	# Logga il "master_user"
	def login(self):
		print("%sLogging in as '%s'..."%(time_log(),self.username))
		if self.test_connection():
			print("%sDone!\n"%(time_log()))

	# Parse del file "accounts.txt" per il popolamento della lista "account_list" con gli user da votare
	def populate_users_list(self):
		namefile = "accounts.txt"
		try:	
			try:
				print(time_log() + "Reading file '"+namefile+"'...",end=" ")
				file = open(namefile)
			except:
				print("File not found")
			file = list(file)
			file = file[0].split('\n')
			self.account_list = file[0].split(', ')
			print("Done, account list populated!")
			print("%sFound %s accounts. Scanning them.\n" % (time_log(),str(len(self.account_list))))
			return self.account_list
		except:
			print("%s'%s' is not correctly formatted"%(time_log(),namefile))

	# Restituisce True se "username_b" segue il "master_user"
	def is_followed_by(self, username_b):
		self.followers_list = []
		for raw_user in Steem().get_followers(self.username,'','blog',1000):
			self.followers_list.append(raw_user['follower'])
		# Se 
		if str(username_b) in self.followers_list:
			print("%s'%s' is followed by '%s'"%(time_log(),self.username,username_b))
			return True
		else:
			print("%s'%s' is not followed by '%s'"%(time_log(),self.username,username_b))
			return False

	# Ritorna il numero di "account" presenti nel file "txt" che ti seguono 
	def follower_status(self):
		follower_counter = 0
		for account in self.account_list:
			if account in self.followers_list:
				follower_counter = follower_counter + 1
		percentage = str(follower_counter/len(self.account_list)*100) + "%"
		status = "%s%s/%s (%s) users are following you.\n" % (time_log(),follower_counter,len(self.account_list),percentage)
		print(status)

	# Popola e printa la lista di tutti i post degli "user" votabili dal "master_user"
	def populate_votable_post_list(self):
		for raw_user in self.account_list:
			if user(raw_user).has_recent_posts():
				for raw_post in user(raw_user).get_all_votable_posts():
					self.master_votable_post_list.append(raw_post)
		log = "%sList populated with %s posts.\n" % (time_log(),len(self.master_votable_post_list))
		print(log)

	def votable_post_list(self):
		i = 0
		print("%sVotable posts from your community:"%(time_log()))
		for p in self.master_votable_post_list:
			i = i + 1
			line = ("%s\t%s"%(i,p))
			print(line)

	# Scrive tutti i post votabili dall'"user" in un file esterno
	def write_votable_posts_in_file(self):
		output = open("%sPOSTS.txt"%(today()),"w")
		for votable_post in self.master_votable_post_list:
			output.write("@"+str(votable_post)+"\n")
		output.close()

# Classe "user"
class user:

	# Crea l'istanza dello "user"
	def __init__(self, username):
		# Username dello "user"
		self.username = username
		# Lista di tutti i post votabili dello "user"
		self.votable_post_list = []

	# Restituisce il bilancio di SBD dell'"user"
	def get_sbd_balance(self):
		sbd_balance = Account(self.username)['sbd_balance']
		return sbd_balance

	# Restituisce "True" se l'utente ha scritto almeno 1 post entro 7 giorni, altrimenti "False"
	def has_recent_posts(self):
		# Questo è un prescan più leggero per vedere se l'utente ha scritto recentemente
		print(time_log()+"Prescanning '"+self.username+"' for recent or old posts", end=" ")
		for raw_post in Account(self.username).get_account_history(-1,1000,filter_by=['comment']):
			timedelta = -((datetime.strptime(raw_post["timestamp"],"%Y-%m-%dT%H:%M:%S") - datetime.now()).days)
			if timedelta >= 7:
				print("\r%s'%s' hasn't written anything in 7 days        " % (time_log(),self.username))
				return False
			elif timedelta < 7:
				post_html_string = "%s/%s" % (raw_post["author"],raw_post["permlink"])
				try:
					if Post(post_html_string).is_main_post():
						print("\r%sFound a recent post from '%s' (%s days ago)" % (time_log(),self.username,timedelta))
						return True		
				except:
					print("\r%sFound a broken link from '%s'. Skipping..."%(time_log(),self.username))

	# Popola la lista "self.votable_post_list" contenente tutti i post votabili dello "user"
	def get_all_votable_posts(self):
		print("%sScanning for votable posts from '%s'" % (time_log(),self.username))
		post_counter = 0
		for raw_post in Account(self.username).get_account_history(-1,5000,filter_by=['comment']):
			if (datetime.strptime(raw_post["timestamp"],"%Y-%m-%dT%H:%M:%S") - datetime.now()).days > -7:
				post_html_string = "%s/%s" % (raw_post["author"],raw_post["permlink"])
				try:
					post = Post(post_html_string)
				except:
					print("%sFound a broken link. Continue searching..."%(time_log()))
					continue
				if ((post.is_main_post()) and (post_html_string not in self.votable_post_list)):
					post_counter = post_counter + 1
					print("%s#%s votable post from '%s': %s" % (time_log(),str(post_counter),self.username,post['permlink']))
					self.votable_post_list.append(post_html_string)
			else:
				print("%sFound %s votable posts from '%s'" % (time_log(),str(post_counter),self.username))
				break
		if len(self.votable_post_list) > 0:
			return self.votable_post_list
		else:
			return None

boot()

master = master_user("ilvacca")
master.populate_users_list()
for user_account in master.account_list:
	master.is_followed_by(user_account)
master.follower_status()
master.populate_votable_post_list()
master.votable_post_list()